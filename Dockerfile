FROM mmcomando/mframe AS builder
COPY . .
# Build
RUN dub build


# Final slim image of app
FROM debian:buster
# Dependencies
RUN apt-get update && apt-get install -y \
    libfcgi \
    libtomcrypt1 \
    libpq5
# Application directory
RUN mkdir -p /app
WORKDIR /app
# Copy executable
COPY --from=builder /source/mframe_example .
# Application entrypoint
ENTRYPOINT [ "/app/mframe_example" ]
# Expose default fcgi port of mframe application
EXPOSE 9001
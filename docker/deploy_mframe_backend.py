# Script used to deploy application based on mframe framework
# Ex. python3 deploy_mframe_backend.py /web/mframe https://gitlab.com/mmcomando/mframe.git
import argparse
import os
import subprocess

# Script arguments
parser = argparse.ArgumentParser(description='Deploy application based on mframe framework.')
parser.add_argument("deploy_path", help="Directory in which project files will be saved")
parser.add_argument("git_repo", help="Git repo clone URL")
args = parser.parse_args()

# Parse aguments
deploy_path_split = os.path.split(args.deploy_path)

proj_path = args.deploy_path + '/'
proj_name = deploy_path_split[1]
proj_git_repo = args.git_repo

# Script

if os.path.isdir(proj_path) == False:
    print('Project does not exist, path(%s)' % proj_path)
    subprocess.check_call(['git', 'clone', proj_git_repo, proj_path])

print('Git pull')
subprocess.check_call(['git', '-C', proj_path, 'pull'])

env_file = '%s.env' % proj_path
if os.path.isfile(env_file) == False:
    print()
    print()
    print('ERROR: Env file does not exist, env_file(%s).' % env_file)
    print('Log in to server and create ".env" file with proper configuration options.')
    print('Check out ".env.example" file for an example.')
    exit(1)

print('Build and push backend image')
cwd = proj_path
subprocess.check_call(['docker-compose', '-f','docker-compose.deploy.yml', 'build'], cwd=cwd)
subprocess.check_call(['docker-compose', '-f','docker-compose.deploy.yml', 'push'], cwd=cwd)

print('Update running images')
cwd = proj_path
subprocess.check_call(['docker-compose', '-f', 'docker-compose.deploy.yml', 'up', '-d', '--force'], cwd=cwd)
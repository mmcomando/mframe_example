# sudo apt install tesseract-ocr tesseract-ocr-pol
# pip3 install numpy
# pip3 install opencv-python
# pip3 install imutils
# pip3 install pytesseract
# pip3 install matplotlib
# pip3 install scikit-image

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import json
import string
from skimage import morphology
from skimage.measure import label

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

import recipe_extract_from_image
import recipe_text_tools

fix_table = {
    # 'Eutecz 2 Cze 4009':'Bułecz Z Cze 400g',
    # 'Rogal 7 Days 1106':'Rogal 7 Days 110g',
    # 'Hielo up szun 200g':'Mielo wp szyn 500g',
    # 'EarszCzerWiniar60g':'BarszCzerWiniar60g',
    # 'Lleser Des. Mix200g':'Deser Des. Mix200g',
    # 'Fon malin Pols luz':'Pon malin Pols luz',
    # 'Chleb rodzinny410q':'Chleb rodzinny410q',
    # 'Fomarancze Hes Luz':'Pomarańcze Des Luz',
    # 'Nandarynka luz':'Mandarynka luz',
    # 'KinderCountr 23,99':'KinderCountr 23,5g',
    # 'DES AVVEG.MIX. 150G':'DES.AVVEG.MIX. 150G',
    # 'F LechFreePoned, SL':'P LechFreePome0,5l',
    # 'PrzyGyroKanis33q':'PrzyGyroKamis33g',
}

def fix_text(text):
    return fix_table.get(text, text)



def getStringFromAnalyzedText(text):
    return list(sorted(  text.split('\n'), key = len, reverse = True))[0]

class ContourData:
    def __init__(self, reader, bb, img):
        self.reader = reader
        self.bb = bb
        self.img = img
        self.text = None
        self.img_text = None


    def get_text(self):
        if self.text is not None:
            return self.text
        
        self.get_contour_text()
        return self.text
    
    def image_smoothening(self, img):
        ret1, th1 = cv2.threshold(img, BINARY_THREHOLD, 255, cv2.THRESH_BINARY)
        ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        blur = cv2.GaussianBlur(th2, (1, 1), 0)
        ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        return th3

    def remove_noise_and_smooth(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        imgB = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 41, 3)
        # imgB = cv2.erode(imgB,np.ones((3, 3), np.uint8),iterations = 1)
        imgB = cv2.morphologyEx(imgB, cv2.MORPH_CLOSE, np.ones((1, 1), np.uint8))
        imgB = cv2.morphologyEx(imgB, cv2.MORPH_OPEN, np.ones((1, 1), np.uint8))

        img = self.image_smoothening(img)

        or_image = cv2.bitwise_or(img, imgB)
        return or_image

    def getBordered(self, img, bb_rec, expand, expand_copy):
        img = self.remove_noise_and_smooth(img)
        img_width = img.shape[1]
        img_height = img.shape[0]

        expand_half = int(expand/2)
        xl = max(0, bb_rec[0] - expand_half)
        xr = min(img_width, bb_rec[0] + bb_rec[2] + expand_half)
        yt = max(0, bb_rec[1] - expand_half)
        yb = min(img_height, bb_rec[1] + bb_rec[3] + expand_half)


        img_part = img[yt:yb, xl:xr].copy()

        top, bottom, left, right = [expand_copy]*4
        return cv2.copyMakeBorder(img_part, top, bottom, left, right, cv2.BORDER_CONSTANT, value=255)
        # return cv2.copyMakeBorder(img_part, top, bottom, left, right, cv2.BORDER_REPLICATE)

    def reset_bb(self, bb):
        self.bb = bb
        self.text = None

    def get_contour_text(self):
        (x, y, w, h) = self.bb
        ar = w / float(h)
        crWidth = w / float(self.img.shape[1])
        middle = (x + int(w/2), y + int(h/2))

        # roiIMG = self.getBordered(self.img, self.bb, uu.f40(10), uu.f40(15))
        roiIMG = self.getBordered(self.img, self.bb, 20, 30)

        config = ''
        # config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789'
        # strr = pytesseract.image_to_string(roiIMG, lang='pl', config=config)
        strr = pytesseract.image_to_string(roiIMG, config=config)
        strr = strr.strip()
        printable = set(string.printable)
        strr = ''.join(filter(lambda x: x in printable, strr))

        self.text = getStringFromAnalyzedText(strr)
        self.img_text = roiIMG

        self.show_img()

    def show_img(self):
        self.get_text()
        if self.img_text is None:
            return
        self.reader.my_img_show("txt(%s)" % self.text, self.img_text)
        cv2.waitKey(0) 
        cv2.destroyAllWindows() 


class Product:
    def __init__(self, reader):
        self.reader = reader

        self.name = ''
        self.tax_code = ''
        self.number = ''
        self.price = 0
        self.discount = 0

        self.cnt_name = None
        self.cnt_tax_code = None
        self.cnt_price = None
        self.cnt_discount = None

    def parse(self):
        if self.cnt_name:
            self.parse_name()
        # if self.cnt_tax_code:
            # self.parse_tax_code() # OCR somehow doesn't work for one char
        if self.cnt_price:
            self.parse_price()
        if self.cnt_discount:
            self.parse_discount()

    def parse_name(self):
        txt = self.cnt_name.get_text()
        txt = fix_text(txt)
        self.name = txt

    def parse_tax_code(self):
        txt = self.cnt_tax_code.get_text()
        self.tax_code = txt

    def parse_price(self):
        txt = self.cnt_price.get_text()
        price, tax_code = recipe_text_tools.get_price(txt)
        if tax_code != '':
            self.tax_code = tax_code
        self.price = price

    def parse_discount(self):
        txt = self.cnt_discount.get_text()
        price, tax_code = recipe_text_tools.get_price(txt)
        if price > 0:
            price = -price
        self.discount = price

    def write_txt_to_img(self, cnt, txt):
        cv2.putText(self.reader.image_recipe_show, txt, (cnt.bb[0], cnt.bb[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
    
    def write_txts_to_img(self):
        if self.cnt_name:
            self.write_txt_to_img(self.cnt_name, self.name)
            # self.write_txt_to_img(self.cnt_name, self.cnt_name.get_text())
        if self.cnt_tax_code:
            self.write_txt_to_img(self.cnt_tax_code, self.tax_code)
            # self.write_txt_to_img(self.cnt_tax_code, self.cnt_tax_code.get_text())
        if self.cnt_price:
            self.write_txt_to_img(self.cnt_price, str(self.price))
            # self.write_txt_to_img(self.cnt_price, str(self.cnt_price.get_text()))
        if self.cnt_discount:
            self.write_txt_to_img(self.cnt_discount, str(self.discount))
            # self.write_txt_to_img(self.cnt_discount, str(self.cnt_discount.get_text()))




test = True
class RecipeDataReader:
    def __init__(self, image_path, silent = False):
        self.silent = silent
        # load the image, resize it, and convert it to grayscale
        # Assume that image width is width of recipe
        self.image = cv2.imread(image_path)
        image_recipe = recipe_extract_from_image.extract_recipe(self.image)
        if image_recipe is not False:
            self.image = image_recipe
        # print(self.image.shape[1])
        # self.image = imutils.resize(self.image, width=1000)
        # print(self.image.shape[1])
        self.image_recipe_show = self.image.copy()


        receipt_width_to_line_height_ratio = 0.05
        self.line_height = self.image.shape[1] * receipt_width_to_line_height_ratio
        self.base_40_ratio = self.line_height / 40 # Parameter when line height is 40
        p_blur = self.round_up_to_odd(3*self.base_40_ratio)

        self.my_print("self.line_height(%d)" % self.line_height)
        self.my_print("p_blur(%d)" % p_blur)


        self.image_gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        # smooth the self.image using a 3x3 Gaussian, then apply the blackhat
        # morphological operator to find dark regions on a light background
        self.image_gray = cv2.GaussianBlur(self.image_gray, (p_blur, p_blur), 0)

        self.image_width = self.image_gray.shape[1]
    
    def f40(self, f):
        return int(f*self.base_40_ratio)

    def f40_odd(self, f):
        f = int(np.ceil(f*self.base_40_ratio))
        return f + 1 if f % 2 == 0 else f

    def round_up_to_odd(self, f):
        f = int(np.ceil(f))
        return f + 1 if f % 2 == 0 else f

    def get_middle(self, cnt):
        (x, y, w, h) = cnt.bb
        middle = (x + int(w/2), y + int(h/2))
        return middle

    def my_img_show(self, title, img, width = 0):
        if self.silent == True:
            return
        if width != 0:
            img = img.copy()
            img = imutils.resize(img, width=800)
        cv2.imshow(title, img)

    def my_print(self, text):
        if self.silent == True:
            return
        print(text)


    def prepare_contours(self):
        self.contours = self.find_text_countours()

        self.find_header()
        self.find_footer()
        self.find_total_price()

        self.min_y = 0
        self.max_y = self.image.shape[0]
        
        if self.cnt_header is not False:
            header_bb = self.cnt_header.bb
            self.min_y = header_bb[1] - self.line_height # in Biedronka date is above 'paragon fiskalny' text
        if self.cnt_footer is not False:
            footer_bb = self.cnt_footer.bb
            self.max_y = footer_bb[1]

        self.my_print("---")
        self.my_print(self.min_y)
        self.my_print(self.max_y)

        def filter_not_between_header_footer(cnt):
            (x, y, w, h) = cnt.bb
            if y < self.min_y:
                return False
            if y > self.max_y:
                return False
            return True

        self.contours = filter(filter_not_between_header_footer, self.contours)
        self.contours = list(self.contours)


    def find_text_countours(self):

        rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (self.f40_odd(13), self.f40_odd(5)))
        sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (self.f40_odd(30), self.f40_odd(3)))
        blackhat = cv2.morphologyEx(self.image_gray, cv2.MORPH_BLACKHAT, rectKernel)


        # compute the Scharr gradient of the blackhat self.image and scale the
        # result into the range [0, 255]
        gradX = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
        gradX = np.absolute(gradX)
        # gradX = blackhat
        (minVal, maxVal) = (np.min(gradX), np.max(gradX))
        gradX = (255 * ((gradX - minVal) / (maxVal - minVal))).astype("uint8")

        # apply a closing operation using the rectangular kernel to close
        # gaps in between letters -- then apply Otsu's thresholding method
        gradX = cv2.morphologyEx(gradX, cv2.MORPH_CLOSE, rectKernel)
        self.image_thresh = cv2.threshold(gradX, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]


        # perform another closing operation, this time using the square
        # kernel to close gaps between lines of the MRZ, then perform a
        # series of erosions to break apart connected components
        self.image_thresh = cv2.morphologyEx(self.image_thresh, cv2.MORPH_CLOSE, sqKernel)
        self.image_thresh = cv2.morphologyEx(self.image_thresh, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_RECT, (self.f40(20), self.f40_odd(5) )))
        self.image_thresh = cv2.morphologyEx(self.image_thresh, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (self.f40(60), self.f40_odd(3))))
        # self.image_thresh = cv2.erode(self.image_thresh, None, iterations=5)

        self.my_img_show("Image", self.image, 800)
        # self.my_img_show("gradXShow", gradX, 800)
        self.my_img_show("image_thresh", self.image_thresh, 800)

        def sort_func(contour):
            (x, y, w, h) = cv2.boundingRect(contour)
            return y

        # find contours in the thresholded self.image and sort them by their
        # size
        cnts = cv2.findContours(self.image_thresh.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        # cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
        cnts = sorted(cnts, key=sort_func)

        contours = []
        for cnt in cnts:
            contours.append(ContourData(self, cv2.boundingRect(cnt), self.image))

        return contours

    def filter_countours_basic(self):
        contours_new = []

        max_h = self.line_height/2
        for cnt in self.contours:
            (x, y, w, h) = cnt.bb
            ar = w / float(h)

            if ar < 1.5:
                self.my_print("ar(%f)" %(ar))
                continue
            if ar > 20:
                self.my_print("ar(%f)" %(ar))
                continue
            if h < self.line_height/3:
                self.my_print("h(%f) maxh(%f)" %(h, max_h))
                continue
            contours_new.append(cnt)

        self.contours = contours_new

    def get_lines_left_right(self):
        products = []

        for iii, cntA in enumerate(self.contours):
            (x, y, w, h) = cntA.bb
            ar = w / float(h)
            crWidth = w / float(self.image_gray.shape[1])
            middle = self.get_middle(cntA)

            for kkk, cntB in enumerate(self.contours):
                if kkk < iii:
                    continue
                (x2, y2, w2, h2) = cntB.bb
                middle2 = (x2 + int(w2/2), y2 + int(h2/2))
                if middle2 == middle:
                    continue

                at_same_hight = abs(middle[1] - middle2[1]) < self.line_height/2
                if not at_same_hight:
                    continue
                cntA_on_left = middle[0] < middle2[0]
                if cntA_on_left:
                    products.append([cntA, cntB])
                else:
                    products.append([cntB, cntA])
        return products

    def find_date_contour(self, date_height_mark):
        self.contour_date = None
        for i, pr in enumerate(self.lines):
            (x, y, w, h) = pr[0].bb

            if y > date_height_mark:
                continue

            self.lines.pop(i)
            self.contour_date = pr[0]

            return


    def find_header(self):
        self.cnt_header = False

        for i, cnt in enumerate(self.contours):
            (x, y, w, h) = cnt.bb
            middle = self.get_middle(cnt)
            inMiddle = (abs(self.image_width/2 - middle[0]) / self.image_width) < 0.1 # is centred
            wide = (middle[0] / self.image_width) > 0.5 # is wide, text 'Paragon Fiskalny' is wide
            if inMiddle == False or wide is False:
                # print("%d %d" % (inMiddle, wide))
                continue

            text = cnt.get_text().lower()

            proper_text_in = 'parag' in text or 'fisk' in text # look for text 'Paragon Fiskalny'
            if not proper_text_in:
                continue

            self.my_print("self.cnt_header(%s)" % (text))
            self.cnt_header = cnt
            self.contours.pop(i)
            return cnt

    def find_footer(self):
        self.cnt_footer = False

        for i, cnt in enumerate(self.contours):
            (x, y, w, h) = cnt.bb
            on_left = (x+w) < (self.image_width/2) # on left side
            if on_left == False :
                # print("%d" % (on_left))
                continue

            text = cnt.get_text().lower()
            proper_text_in = 'suma pln' in text or 'uha pl' in text    # look for text 'SUMA PLN'
            if not proper_text_in:
                continue

            self.my_print("self.cnt_footer(%s)" % (text))
            self.cnt_footer = cnt
            self.contours.pop(i)
            return

    def find_total_price(self):
        self.cnt_total_price = False

        if self.cnt_footer is False:
            return

        middle_footer = self.get_middle(self.cnt_footer)

        for i, cnt in enumerate(self.contours):
            (x, y, w, h) = cnt.bb
            middle = self.get_middle(cnt)
            at_same_hight = abs(middle[1] - middle_footer[1]) < self.line_height/2
            on_right = (x+w) > (self.image_width/2) # on left side
            if on_right == False :
                continue
            if at_same_hight == False :
                continue
                
            self.cnt_total_price = cnt
            self.contours.pop(i)
            return

    def classify_lines(self):
        found_tax_word = False
        products_lines = []
        tax_lines = []
        for i, line in enumerate(self.lines):
            cnt = line[0]
            (x, y, w, h) = cnt.bb

            
            if found_tax_word == False:
                text = cnt.get_text().lower()
                found_tax_word = 'opodat' in text

            if found_tax_word == True:
                tax_lines.append(line)
            else:
                products_lines.append(line)

        width_border = int(self.image_width * 0.02)
        xx_product = width_border
        width_product = int(self.image_width * 0.4)
        xx_price = int(self.image_width * 0.6)
        width_price = self.image_width - xx_price - width_border
        xx_tax_code = int(self.image_width * 0.47)
        width_tax_code = int(self.image_width * 0.05)

        for i, product in enumerate(products_lines):
            product[0].reset_bb((xx_product, product[0].bb[1], width_product, product[0].bb[3]))
            product[1].reset_bb((xx_price, product[1].bb[1], width_price, product[1].bb[3]))

            bb_tax = (xx_tax_code, product[0].bb[1], width_tax_code, product[0].bb[3])
            cnt_tax = ContourData(self, bb_tax, self.image)
            # cnt_tax.show_img()
            product.append(cnt_tax)

        self.products_lines = products_lines
        self.tax_lines = tax_lines

    def process_product_lines(self):
        for i, product_line in enumerate(self.products_lines):
            if i == 0:
                continue
            if self.products_lines[i-1] is None:
                self.my_print('ERR: two discounts in a row')
                continue

            text = product_line[0].get_text().lower()
            if 'rabat' not in text:
                continue
            
            self.products_lines[i] = None
            text_value = product_line[1].get_text().lower()
            self.products_lines[i-1].append(product_line[1])


        self.products_lines = [pp for pp in self.products_lines if pp]

        products = []
        for i, product_line in enumerate(self.products_lines):
            product = Product(self)
            product.cnt_name = product_line[0]
            product.cnt_tax_code = product_line[2]
            product.cnt_price = product_line[1]
            if len(product_line) == 4:
                product.cnt_discount = product_line[3]
            products.append(product)

        self.products = products
   

    def parse(self):
        ret_data = {
            "date": "",
            "price_total": "",
            "products":[]
        }

        # Line has three contours [contour_on_right, contour_on_left]
        self.lines = self.get_lines_left_right()
        self.find_date_contour(self.min_y + self.line_height)
        self.classify_lines()
        self.process_product_lines()
        # self.find_line_height()


        for product in self.products:
            product.parse()
            product.write_txts_to_img()

            data = {"name":product.name, "price":product.price, "discount":product.discount}
            ret_data["products"].append(data)
            # print(data)

        if self.contour_date is not None:
            ret_data["date"] = self.contour_date.get_text(),
        if self.cnt_total_price is not False:
            price, tax_code = recipe_text_tools.get_price(self.cnt_total_price.get_text())
            ret_data["price_total"] = price

        return ret_data
    
    # def find_line_height(self):
    #     positions_y = []
    #     for line in self.lines:
    #         middle = self.get_middle(line[1])
    #         positions_y.append(middle[1])
        
    #     positions_y = sorted(positions_y)

    #     subsequent_lines_start_max = 0
    #     subsequent_lines_start_max_count = 0
    #     subsequent_lines_start = 0
    #     subsequent_lines_end = 0

    #     for i, y in enumerate(positions_y):
    #         if i == 0:
    #             continue
    #         dt_lines = abs(y-positions_y[i-1])
    #         is_subsequent_line = abs(dt_lines - self.line_height) < self.line_height / 4

    #         if is_subsequent_line:
    #             subsequent_lines_end = i
    #             count = subsequent_lines_end - subsequent_lines_start
    #             if count > subsequent_lines_start_max_count:
    #                 subsequent_lines_start_max = subsequent_lines_start
    #                 subsequent_lines_start_max_count = count
    #             continue

    #         subsequent_lines_start = i
    #         subsequent_lines_end = i
        
    #     if subsequent_lines_start_max_count == 0:
    #         self.my_print('no subsequent lines found')
    #         self.computed_line_height = self.line_height
    #         return

    #     dt_y = positions_y[subsequent_lines_start_max+subsequent_lines_start_max_count] - positions_y[subsequent_lines_start_max]
    #     height = dt_y / subsequent_lines_start_max_count
    #     self.my_print('height(%s)' % (height))
    #     self.computed_line_height = int(height)
        



import tempfile

import cv2
import numpy as np
from PIL import Image

IMAGE_SIZE = 1800
BINARY_THREHOLD = 180


# image = cv2.imread('test.png', 0)

# def image_smoothening(img):
#     ret1, th1 = cv2.threshold(img, BINARY_THREHOLD, 255, cv2.THRESH_BINARY)
#     ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
#     blur = cv2.GaussianBlur(th2, (1, 1), 0)
#     ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
#     return th3

# def remove_noise_and_smooth(img):
#     filtered = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 41, 3)

#     imgB = filtered
#     # imgB = cv2.erode(imgB,np.ones((3, 3), np.uint8),iterations = 1)
#     imgB = cv2.morphologyEx(imgB, cv2.MORPH_CLOSE, np.ones((3, 3), np.uint8))
#     imgB = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, np.ones((1, 1), np.uint8))

#     img = image_smoothening(img)

#     or_image = cv2.bitwise_or(img, imgB)
#     return or_image

# image_A = remove_noise_and_smooth(image)
# cv2.imshow('image_A', image_A)
# cv2.waitKey(0)

# exit
# safdas




test = False
recipe_extract_from_image.test = test
image_path = '20210128_194405'
image_path_in = '%s.jpg' % image_path
image_path_out = '%s_extracted.jpg' % image_path
image_path_out_mask = '%s_extracted_mask.jpg' % image_path
image_path_out_img_text = '%s_extracted_img_text.jpg' % image_path

if test == False: 
    ap = argparse.ArgumentParser()
    ap.add_argument("--image_in", required = True, help = "Path to the image with recipe")
    ap.add_argument("--image_out", required = True, help = "Path to extracted recipe image")
    ap.add_argument("--image_out_mask", required = True, help = "Path to ouputed mask image")
    ap.add_argument("--image_out_img_text", required = True, help = "Path to ouputed image with text image")
    args = vars(ap.parse_args())

    image_path_in = args["image_in"]
    image_path_out = args["image_out"]
    image_path_out_mask = args["image_out_mask"]
    image_path_out_img_text = args["image_out_img_text"]

recipe_data_reader = RecipeDataReader(image_path_in, not test)
recipe_data_reader.prepare_contours()
ret_data = recipe_data_reader.parse()

cv2.imwrite(image_path_out, recipe_data_reader.image) 
cv2.imwrite(image_path_out_mask, recipe_data_reader.image_thresh) 
cv2.imwrite(image_path_out_img_text, recipe_data_reader.image_recipe_show)

# print(ret_data["products"])

# convert into JSON:
y = json.dumps(ret_data)

# the result is a JSON string:
print(y) 
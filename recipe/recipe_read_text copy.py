# sudo apt install tesseract-ocr tesseract-ocr-pol
# pip3 install numpy
# pip3 install opencv-python
# pip3 install imutils
# pip3 install pytesseract
# pip3 install matplotlib
# pip3 install scikit-image

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import json

from skimage import morphology
from skimage.measure import label

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

import recipe_extract_from_image

# initialize a rectangular and square structuring kernel

# Constant
receipt_width_to_line_height_ratio = 0.05
# Per image
p_line_height = 0
p_base_40_ratio = 0
p_blur = 0

test = True
class RecipeDataReader:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart

def f40(f):
    global p_line_height, p_base_40_ratio
    return int(f*p_base_40_ratio)

def f40_odd(f):
    f = int(np.ceil(f*p_base_40_ratio))
    return f + 1 if f % 2 == 0 else f

def round_up_to_odd(f):
    f = int(np.ceil(f))
    return f + 1 if f % 2 == 0 else f

def get_line_height():
    global p_line_height
    return p_line_height


def my_img_show(title, img, width = 0):
    global test
    if test == False:
        return
    if width != 0:
        img = img.copy()
        img = imutils.resize(img, width=800)
    cv2.imshow(title, img)

def my_print(text):
    global test
    if test == False:
        return
    print(text)


def find_text_countours(image, gray):

    rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (f40_odd(13), f40_odd(5)))
    sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (f40_odd(30), f40_odd(3)))
    blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKernel)




    # compute the Scharr gradient of the blackhat image and scale the
    # result into the range [0, 255]
    gradX = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
    gradX = np.absolute(gradX)
    # gradX = blackhat
    (minVal, maxVal) = (np.min(gradX), np.max(gradX))
    gradX = (255 * ((gradX - minVal) / (maxVal - minVal))).astype("uint8")

    # apply a closing operation using the rectangular kernel to close
    # gaps in between letters -- then apply Otsu's thresholding method
    gradX = cv2.morphologyEx(gradX, cv2.MORPH_CLOSE, rectKernel)
    thresh = cv2.threshold(gradX, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]


    # perform another closing operation, this time using the square
    # kernel to close gaps between lines of the MRZ, then perform a
    # series of erosions to break apart connected components
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sqKernel)
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_RECT, (f40(20), f40_odd(5) )))
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (f40(60), f40_odd(3))))
    # thresh = cv2.erode(thresh, None, iterations=5)

    my_img_show("Image", image, 800)
    # my_img_show("gradXShow", gradX, 800)
    my_img_show("thresh", thresh, 800)

    def sort_func(contour):
        (x, y, w, h) = cv2.boundingRect(contour)
        return y

    # find contours in the thresholded image and sort them by their
    # size
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    # cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    cnts = sorted(cnts, key=sort_func)
    return {"thresh":thresh, "cnts": cnts}


def getBordered(img, bb_rec, expand, expand_copy):
    imgWidth = img.shape[1]
    imgHeight = img.shape[0]

    expand_half = int(expand/2)
    xl = max(0, bb_rec[0] - expand_half)
    xr = min(imgWidth, bb_rec[0] + bb_rec[2] + expand_half)
    yt = max(0, bb_rec[1] - expand_half)
    yb = min(imgHeight, bb_rec[1] + bb_rec[3] + expand_half)


    img_part = img[yt:yb, xl:xr].copy()

    top, bottom, left, right = [expand_copy]*4
    return cv2.copyMakeBorder(img_part, top, bottom, left, right, cv2.BORDER_REPLICATE)



def get_contour_text(image, gray, c, shoow_image=True):
    bb_rec = cv2.boundingRect(c)
    (x, y, w, h) = bb_rec
    ar = w / float(h)
    crWidth = w / float(gray.shape[1])
    middle = (x + int(w/2), y + int(h/2))
    # print(y)


    roiIMG = getBordered(image, bb_rec, f40(10), f40(15))

    strr = pytesseract.image_to_string(roiIMG).strip()

    if shoow_image:
        my_img_show("ROI" + str(y), roiIMG)
        my_print("text(%s)" % (strr))
     
    return{
        "text":strr,
        "pos":(x, y+int(h*2/3)),
    }

def getMiddle(countour):
    (x, y, w, h) = cv2.boundingRect(countour)
    middle = (x + int(w/2), y + int(h/2))
    return middle

def get_header(image, gray, cntsNew):
    imgWidth = gray.shape[1]

    for c in cntsNew:
        (x, y, w, h) = cv2.boundingRect(c)
        middle = getMiddle(c)
        inMiddle = (abs(imgWidth/2 - middle[0]) / imgWidth) < 0.1 # is centred
        wide = (middle[0] / imgWidth) > 0.5 # is wide, text 'Paragon Fiskalny' is wide
        if inMiddle == False or wide is False:
            # print("%d %d" % (inMiddle, wide))
            continue

        ret_data = get_contour_text(image, gray, c, False)
        text = ret_data['text'].lower()

        proper_text_in = 'parag' in text or 'fisk' in text # look for text 'Paragon Fiskalny'
        if not proper_text_in:
            continue

        my_print("header(%s)" % (text))
        return c

    return False

def get_footer(image, gray, cntsNew):
    imgWidth = gray.shape[1]

    for c in cntsNew:
        (x, y, w, h) = cv2.boundingRect(c)
        on_left = (x+w) < (imgWidth/2) # on left side
        if on_left == False :
            # print("%d" % (on_left))
            continue

        ret_data = get_contour_text(image, gray, c, False)
        text = ret_data['text'].lower()

        proper_text_in = 'suma pln' in text # look for text 'SUMA PLN'
        if not proper_text_in:
            continue

        my_print("footer(%s)" % (text))
        return c

    return False

def filter_countours(cnts, img_width):
    cntsNew = []

    max_h = get_line_height()/2

    for c in cnts:
        (x, y, w, h) = cv2.boundingRect(c)
        ar = w / float(h)
        crWidth = w / float(img_width)

        if ar < 1.5:
            my_print("ar(%f)" %(ar))
            continue
        if ar > 20:
            my_print("ar(%f)" %(ar))
            continue
        if h < get_line_height()/3:
            my_print("h(%f) maxh(%f)" %(h, max_h))
            continue
        cntsNew.append(c)
    return cntsNew

def get_product_price_pairs(gray, cntsNew):
    products = []

    iii = -1
    for c in cntsNew:
        iii += 1
        (x, y, w, h) = cv2.boundingRect(c)
        ar = w / float(h)
        crWidth = w / float(gray.shape[1])
        middle = getMiddle(c)

        kkk = -1
        for c2 in cntsNew:
            kkk += 1
            if kkk < iii:
                continue
            (x2, y2, w2, h2) = cv2.boundingRect(c2)
            middle2 = (x2 + int(w2/2), y2 + int(h2/2))
            if middle2 == middle:
                continue

            at_same_hight = abs(middle[1] - middle2[1]) < get_line_height()/2
            if not at_same_hight:
                continue
            c_on_left = middle[0] < middle2[0]
            if c_on_left:
                products.append([c, c2])
            else:
                products.append([c2, c])
    return products

def get_date_contour(products, date_height_mark):
    for i, pr in  enumerate(products):
        (x, y, w, h) = cv2.boundingRect(pr[0])

        if y > date_height_mark:
            continue

        products.pop(i)
        return pr[0]

    return None
    
def parse_image(imagePath):
    global p_line_height, p_base_40_ratio
    my_print("imagePath(%s)" % imagePath)

    # load the image, resize it, and convert it to grayscale
    # Assume that image width is width of recipe
    image = cv2.imread(imagePath)
    image_recipe = recipe_extract_from_image.extract_recipe(image)
    if image_recipe is not False:
        image = image_recipe
    # image = imutils.resize(image, width=800)
    imageShow = image.copy()


    receipt_width_to_line_height_ratio = 0.05
    p_line_height = image.shape[1] * receipt_width_to_line_height_ratio
    p_base_40_ratio = p_line_height / 40 # Parameter when line height is 40
    p_blur = round_up_to_odd(3*p_base_40_ratio)

    my_print("p_line_height(%d)" % p_line_height)
    my_print("p_blur(%d)" % p_blur)


    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # smooth the image using a 3x3 Gaussian, then apply the blackhat
    # morphological operator to find dark regions on a light background
    gray = cv2.GaussianBlur(gray, (p_blur, p_blur), 0)


    # loop over the contours
    cnts_data = find_text_countours(image, gray)
    cnts = cnts_data["cnts"]
    cnts = filter_countours(cnts, gray.shape[1])
    header = get_header(image, gray, cnts)
    footer = get_footer(image, gray, cnts)

    min_y = 0
    max_y = image.shape[0]
    
    if header is not False:
        header_bb = cv2.boundingRect(header)
        min_y = header_bb[1] - p_line_height # in Biedronka date is above 'paragon fiskalny' text
    if footer is not False:
        footer_bb = cv2.boundingRect(footer)
        max_y = footer_bb[1]

    my_print("---")
    my_print(min_y)
    my_print(max_y)

    def filter_not_between_header_footer(cnt):
        (x, y, w, h) = cv2.boundingRect(cnt)
        if y < min_y:
            return False
        if y > max_y:
            return False
        return True

    cnts = filter(filter_not_between_header_footer, cnts)
    cnts = list(cnts)

    products = []
    products = get_product_price_pairs(gray, cnts)
    date_contour = get_date_contour(products, min_y + p_line_height)

    date = get_contour_text(image, gray, date_contour, True)["text"]
    my_print(len(products))

    # show_img = True
    show_img = False
    def analyzePart(c):
        ret_data = get_contour_text(image, gray, c, show_img)
        cv2.putText(imageShow, ret_data["text"], ret_data["pos"], cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        return ret_data["text"]

    # for cc in cnts:
    #     analyzePart(cc)
    def getStringFromAnalyzedText(text):
        return list(sorted(  text.split('\n'), key = len, reverse = True))[0]

    prod_data = []
    for pp in products:
        text = ['', '']        
        text[0] = getStringFromAnalyzedText(analyzePart(pp[0]))
        text[1] = getStringFromAnalyzedText(analyzePart(pp[1]))
        prod_data.append(text)

    my_print('')
    my_print('')

    ret_data = {
        "date": date,
        "extracted": image_recipe,
        "mask": cnts_data["thresh"],
        "img_text": imageShow,
        "products":[]
    }

    for tt in prod_data:
        my_print("name(%s) price(%s)" % (tt[0], tt[1]))
        ret_data["products"].append({"name":tt[0], "price":tt[1]})


    # show the output images
    # my_img_show("imageShow", imageShow, 800)
    # cv2.waitKey(0)


    return ret_data
    

# test = False
recipe_extract_from_image.test = test
image_path_in = 'img.jpg'
image_path_out = 'img_extracted.jpg'
image_path_out_mask = 'img_extracted_mask.jpg'
image_path_out_img_text = 'img_extracted_img_text.jpg'

# if test == False: 
#     ap = argparse.ArgumentParser()
#     ap.add_argument("--image_in", required = True, help = "Path to the image with recipe")
#     ap.add_argument("--image_out", required = True, help = "Path to extracted recipe image")
#     ap.add_argument("--image_out_mask", required = True, help = "Path to ouputed mask image")
#     ap.add_argument("--image_out_img_text", required = True, help = "Path to ouputed image with text image")
#     args = vars(ap.parse_args())

#     image_path_in = args["image_in"]
#     image_path_out = args["image_out"]
#     image_path_out_mask = args["image_out_mask"]
#     image_path_out_img_text = args["image_out_img_text"]

image = cv2.imread(image_path_in)
ret_data = parse_image(image_path_in)

cv2.imwrite(image_path_out, ret_data["extracted"]) 
cv2.imwrite(image_path_out_mask, ret_data["mask"]) 
cv2.imwrite(image_path_out_img_text, ret_data["img_text"])

# print(ret_data["products"])





del ret_data["extracted"]
del ret_data["mask"]
del ret_data["img_text"]
# convert into JSON:
y = json.dumps(ret_data)

# the result is a JSON string:
print(y) 
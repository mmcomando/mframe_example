# pip3 install shapely


# import the necessary packages
# from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
import numpy as np
import argparse
import cv2
import imutils
import math


import numpy as np
from shapely.geometry import Point
from shapely.geometry import LineString

test = True

def my_print(text):
    global test
    if test == False:
        return
    print(text)

def my_img_show(title, img, width = 0):
    global test
    if test == False:
        return
    if width != 0:
        img = img.copy()
        img = imutils.resize(img, width=800)
    cv2.imshow(title, img)

def project_point_to_line(m_p, m_l):
    # m_p = float(m_p)
    point = Point(m_p[0], m_p[1])
    line = LineString([(m_l[0][0], m_l[0][1]), (m_l[1][0], m_l[1][1])])

    x = np.array(point.coords[0])

    u = np.array(line.coords[0])
    v = np.array(line.coords[len(line.coords)-1])

    n = v - u
    n /= np.linalg.norm(n, 2)

    P = u + n*np.dot(x - u, n)
    return (int(P[0]), int(P[1]))



def extract_recipe(image):
    ratio = image.shape[0] / 500.0
    orig = image.copy()
    image = imutils.resize(image, height = 500)
    # convert the image to grayscale, blur it, and find edges
    # in the image
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 50, 170)
    # show the original image and the edge detected image
    my_print("STEP 1: Edge Detection")
    my_img_show("Image", image)
    my_img_show("Edged", edged)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # find the contours in the edged image, keeping only the
    # largest ones, and initialize the screen contour
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)


    def vec_len(vec):
        dt_x = vec[0][0] - vec[1][0]
        dt_y = vec[0][1] - vec[1][1]
        return math.sqrt(dt_y*dt_y+dt_x*dt_x)

    def vec_abs_dir(vec):
        v_len = vec_len(vec)
        dt_x = vec[0][0] - vec[1][0]
        dt_y = vec[0][1] - vec[1][1]
        return [abs(dt_x / v_len), abs(dt_y / v_len)]



    def arcLength(c):
        return cv2.arcLength(c, False)

    cnts = sorted(cnts, key = arcLength, reverse = True)[:5]


    lines = []

    maxPeri = 0
    iii = 0
    for c in cnts:
        peri = cv2.arcLength(c, False)
        approx = cv2.approxPolyDP(c, 0.05 * peri, True)

        last_vec = approx[0]
        for a in approx[1:]:
            line = [last_vec[0], a[0]]
            lines.append(line)
            my_print(line)
            # Draw some lines
            image = cv2.line(image, (line[0][0], line[0][1]), (line[1][0], line[1][1]), (iii, 0, 255-iii) ,2) 
            iii += 20
            last_vec = a

    lines = sorted(lines, key = vec_len, reverse = True)[:]


    lines_old = lines
    lines = []
    for line in lines_old:
        if line[0][1] > line[1][1]:
            lines.append([line[0], line[1]])
        else:
            lines.append([line[1], line[0]])

    longest_line = lines[0]
    longest_line_dir = vec_abs_dir(longest_line)

    def filter_by_dir(line):
        if vec_len([line[0], longest_line[0]]) < 50:
            return False # Same starting point

        line_dir = vec_abs_dir(line)
        if vec_len([line_dir, longest_line_dir]) > 0.2:
            return False
        return True 
    my_img_show("Outline", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    lines_candidates = list(filter(filter_by_dir, lines))[:2]
    lines = [longest_line, lines_candidates[0]] 

    if len(lines) != 2:
        my_print('No two long lines pointing to same direction found')
        return False
    lineA = lines[0]
    lineB = lines[1]
    # Project shorter to longer one 
    lineA = [project_point_to_line(lineB[0], lineA), project_point_to_line(lineB[1], lineA)]

    # Make lineA be on left side
    if lineA[0][0] > lineB[0][0]:
        tmp = lineA
        lineA = lineB
        lineB = tmp

    image = cv2.line(image, (lineA[0][0], lineA[0][1]), (lineA[1][0], lineA[1][1]), (0, 0, 0) ,5) 
    image = cv2.line(image, (lineB[0][0], lineB[0][1]), (lineB[1][0], lineB[1][1]), (255, 0, 0) ,5) 

    p1 = [lineA[0][0], lineA[0][1]]
    p2 = [lineA[1][0], lineA[1][1]]
    p3 = [lineB[0][0], lineB[0][1]]
    p4 = [lineB[1][0], lineB[1][1]]
    new_width = int(vec_len( [lineA[0], lineB[0]]))
    new_height = int(vec_len(lineA))

    p1 = [int(i * ratio) for i in p1]   
    p2 = [int(i * ratio) for i in p2]   
    p3 = [int(i * ratio) for i in p3]  
    p4 = [int(i * ratio) for i in p4]  
    new_width = int(new_width * ratio)
    new_height = int(new_height * ratio)

    ppp =[p3, p1, p2, p4]
    ppp =[p1, p3, p4, p2]
    ppp =[p2, p4, p3, p1]
    my_print(ppp)
    src_pts = np.array(ppp, dtype=np.float32)
    dst_pts = np.array([[0, 0],   [new_width, 0],  [new_width, new_height], [0, new_height]], dtype=np.float32)

    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warp = cv2.warpPerspective(orig, M, (new_width, new_height))

    # cv2.imwrite('C:/Users/N/Desktop/Test_gray.jpg', image_gray)

    my_img_show("warp", warp, 800)
    my_img_show("Outline", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return warp




# # test = False
# image_path = 'img'
# image_path = '20210128_194405'
# image_path = '20210128_194423'
# image_path = '20210128_194432'
# image_path = '20210128_194434'
# image_path = '20210128_194438_bad'
# image_path = '20210128_194439_bad'
# image_path = '20210128_194453'
# image_path = '20210128_194502'
# # image_path = '20210128_194518_bad'

# image_path_in = '%s.jpg' % image_path
# image_path_out = '%s_extracted.jpg' % image_path

# # if test == False: 
# #     ap = argparse.ArgumentParser()
# #     ap.add_argument("--image_in", required = True, help = "Path to the image with recipe")
# #     ap.add_argument("--image_out", required = True, help = "Path to extracted recipe image")
# #     args = vars(ap.parse_args())

# #     image_path_in = args["image_in"]
# #     image_path_out = args["image_out"]

# image = cv2.imread(image_path_in)
# extracted_image = extract_recipe(image)

# if image_path_out != '':
#     cv2.imwrite(image_path_out, extracted_image) 

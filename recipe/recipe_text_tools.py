


# return tuple (price, tax_code)
# Ex. 1 1x1,23 1 , 23B
def get_price(txt):
    txt = txt.strip()
    txt = txt.replace(',', '.')
    tax_code = ''

    if txt == '':
        txt = '0'

    ind_dot = txt.rfind('.')
    if ind_dot == -1:
        def_ret = (float(txt), tax_code)
        return def_ret

    ind_space = txt.rfind(' ', 0, ind_dot-1)
    if ind_space != -1:
        txt = txt[ind_space+1:]
        txt = txt.replace(' ', '')



    ind_dot = txt.rfind('.')
    sec_part_len = len(txt) - ind_dot - 1
    if sec_part_len == 0:
        txt += '00'
    if sec_part_len == 1:
        txt += '0'
    elif sec_part_len >= 3:
        tax_code = txt[ind_dot + 3]
        txt = txt[:ind_dot + 3]

    # if tax_code == '8': # OCR error
        # tax_code = 'B'
    if tax_code == '4': # OCR error
        tax_code = 'A'
    if tax_code not in ['A', 'B', 'C']:
        tax_code = ''

    number = float(txt)
    # print(number)
    return (number, tax_code)

def test():
    assert(get_price('123')[0] == 123)
    assert(get_price('123 ')[0] == 123)
    assert(get_price('1,23')[0] == 1.23)
    assert(get_price('AA1,23 1,23')[0] == 1.23)
    assert(get_price('AA1,23 1,23')[0] == 1.23)
    assert(get_price('AA1,23 1, 23')[0] == 1.23)
    assert(get_price('AA1,23 1 , 23')[0] == 1.23)
    assert(get_price('AA1,23 1 , 2384')[0] == 1.23)
    assert(get_price('AA1,23 1 , 238')[1] == 'B')
    assert(get_price('AA1,23 1 , 23A84')[0] == 1.23)
    assert(get_price('AA1,23 1 , 23A84')[1] == 'A')
    assert(get_price('AA1,23 1,2')[0] == 1.20)
    assert(get_price('AA1,23 1,')[0] == 1.00)
    assert(get_price('-0,50)')[0] == -0.50)
    assert(get_price('-0,50)')[1] == '')



# test()
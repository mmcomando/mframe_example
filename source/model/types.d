module model.types;

import std.meta : AliasSeq;

import mframe.schema;

struct UserSessionData
{
    long id;
    string name;
}

// PostgreSQLTableName user is a special table
@PostgreSQLTableName("person")
struct User
{
    ulong id;
    string name;
    string password;
    int age;
}

struct UserRoles
{
    ulong id;
    ulong userId;
    int role;
}

struct Payment
{
    ulong id;
    int value;
    string description;
}

enum ExpenseType : int
{
    NotAssigned,
    Recipe,
}

struct Expense
{
    ulong id;
    ulong userId;
    ulong time;
    int expenseType = ExpenseType.NotAssigned;
    string description;
}

enum ProductType : int
{
    NotAssigned,
    Food,
    Device,
}

struct Product
{
    ulong id;
    string name;
    ulong expenseId;
    double price;
    double discount;
    int productType = ProductType.NotAssigned;
}

private alias StandardSchemaTypes = AliasSeq!(User, UserRoles, Payment, Expense, Product);
enum Schema[] gStandardSchemas = creteSchemas!StandardSchemaTypes;
module model.main_db.user;

import core.stdc.stdio;
import core.thread;
import std.conv : to;
import std.format;
import std.stdio : writeln, writefln;

import postgresql.libpq_fe;

import mframe.db.postgresql;
import model.types;
import serializer.json;
import mframe.schema;

struct PostgresBase
{
    PostgresConnection connection;
    Schema[] schemas;

    void initiaize()
    {
        connection.initiaizeEnv();
        bool ok = connection.createDatabase();
        while(ok == false)
        {
            ok = connection.createDatabase();
            if(ok == false)
            {
                Thread.sleep(10.seconds);
            }
        }
    }

    bool executeQuery(string queryStr)
    {
        try
        {
            // writefln("Execute Query(%s)", queryStr);
            connection.queryPrint(queryStr);
            return true;
        }
        catch (Exception e)
        {
            writeln(e);
        }
        return false;
    }

    bool deleteById(string tableName, ulong id)
    {
        try
        {
            string queryStr = "DELETE FROM ";
            queryStr ~= tableName;
            queryStr ~= " WHERE id=";
            queryStr ~= id.to!string;
            queryStr ~= ";";
            auto ok = connection.queryPrint(queryStr);
        }
        catch (Exception e)
        {
            writeln(e);
            return false;
        }
        return true;
    }

    T getById(T)(string tableName, ulong id)
    {
        return connection.getById!(T)(tableName, id);
    }

    T[] getRecords(T)(string tableName)
    {
        T[] array = connection.getRecords!(T)(tableName);
        return array;
    }

    void setSchemas(Schema[] schemas)
    {
        this.schemas = schemas;
    }

    string[] getPossibleTasks()
    {
        string[] queries;
        foreach (schema; schemas)
        {
            try
            {
                queries ~= connection.getUpdateDatabaseModelQueries(schema);
            }
            catch (Exception e)
            {
                writeln("getUpdateDatabaseModelQueries Exception: ", e);
            }
        }
        return queries;
    }

    bool executeTask(string str)
    {
        return executeQuery(str);
    }


    bool userExists(string name)
    {
        return connection.queryBool("
SELECT EXISTS (
    SELECT id 
    FROM person 
    WHERE name=$1
);
    ", name);
    }

    ulong logInUser(string name, string password)
    {
        PGresultAutoRelese result = connection.queryPrint("
    SELECT id 
    FROM person 
    WHERE name=$1 and password=$2
    ", name, password);

        int rowsNum = PQntuples(result);

        if (rowsNum == 0)
        {
            return 0;
        }
        char* idC = PQgetvalue(result, 0, 0);
        string id = idC[0 .. PQgetlength(result, 0, 0)].idup;
        return id.to!ulong;
    }

    bool registerUser(User user)
    {
        connection.queryPrint("SELECT id, name,age FROM person;");
        if (user.name.length == 0 || user.id != 0)
        {
            writeln("user.name.length == 0 || user.id != 0");
            return false;
        }

        bool userIsRegistred = userExists(user.name);

        if (userIsRegistred)
        {
            writeln("userIsRegistred");
            return false;
        }

        connection.query("INSERT INTO person (name, password, age) VALUES($1, $2, $3);",
                user.name, user.password, user.age);
        connection.queryPrint("SELECT id, name,age FROM person;");
        return true; // TODO check output
    }

    User getUserById(ulong id)
    {
        return getById!(User)("person", id);
    }

}

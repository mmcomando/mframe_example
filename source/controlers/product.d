module controlers.product;

import std.conv;
import std.datetime;
import std.file : write;
import std.process : execute;
import std.path;
import std.stdio;

import mmutils.log;

import mframe.db.postgresql;
import mframe.multipart;
import mframe.request;
import mframe.router;
import mframe.schema;
import mframe.trust;
import model.types;
import serializer.json;






struct ProductController
{
    PostgresConnection connection;
    Schema schema;
    Schema schemaExpense;

    static struct RecipeData
    {
        Product[] products;
    }

    void initialize()
    {
        connection.initiaizeEnv();
        schema = creteSchema!Product;
        schemaExpense = creteSchema!Expense;
    }

    void register(ref Router router)
    {
        router.addJSON("/api/expense/array", &getProducts, TrustLevel.AuthenticatedUser);
        router.addJSON("/api/expense/extract_from_images", &extractFromImages, TrustLevel.AuthenticatedUser);
        router.addJSON("/api/product/array", &getProducts, TrustLevel.AuthenticatedUser);
    }


    Expense[] getExpenses(const RequestData requestData, out ResponseData responseData)
    {

        Expense[] ret = connection.getData!Expense(
            "SELECT * FROM expense WHERE userId=$1::bigint", 
            requestData.userId.to!string
            );

        responseData.statusCode = StatusCode.ok;
        return ret;
    }

    Product[] getProducts(const RequestData requestData, out ResponseData responseData)
    {
        string expenseId = requestData.get.get("expenseid", "0");
        Product[] ret;

        if(expenseId == "0")
        {
            ret = connection.getData!Product("
SELECT product.*
FROM product INNER JOIN expense ON expense.id = product.expenseId
WHERE expense.userId=$1::bigint", 
                        requestData.userId.to!string,
                        );
        }
        else
        {
            ret = connection.getData!Product("
SELECT product.*
FROM product INNER JOIN expense ON expense.id = product.expenseId
WHERE expense.userId=$1::bigint and product.expenseId=$2::bigint", 
                        requestData.userId.to!string, 
                        expenseId,
                        );
        }

        responseData.statusCode = StatusCode.ok;
        return ret;
    }

    void extractFromImages(const RequestData requestData, out ResponseData responseData)
    {
        foreach(ref const(MultipartPart) data; requestData.multipart)
        {
            if(data.header.contentDisposition.name != "file" || data.header.contentDisposition.filename.length == 0)
            {
                log(LL(LogType.Error), "Ignore part");
                continue;
            }
            write(data.header.contentDisposition.filename, data.value);

            RecipeData recipeData = extractRecipeData(data.header.contentDisposition.filename.idup);
            Expense expense;
            expense.time = Clock.currTime().toUnixTime();
            expense.description = data.header.contentDisposition.filename.idup;
            expense.userId = requestData.userId;
            expense.id = connection.queryInsert(schemaExpense, expense);
            foreach(ref Product product; recipeData.products)
            {
                writefln("userid(%s)", requestData.userId);
                product.expenseId = expense.id;
                connection.queryInsert(schema, product);
            }
            writeln(recipeData);
        }
        responseData.statusCode = StatusCode.ok;
    }


    RecipeData extractRecipeData(string imagePath)
    {
        auto perfLog = log_perf(LL(), "Extract End", "Extract Start, path(%.*s)", cast(int)imagePath.length, imagePath.ptr);
        RecipeData recipeData;

        string imagePathExtracted = imagePath.setExtension("_extracted.jpg");
        // string pathScriptExtractRecipe = "./recipe/recipe_extract_from_image.py";
        string pathScriptExtractText = "./recipe/recipe_read_text.py";

        auto extractText = execute(
            [
                "python3",
                pathScriptExtractText,
                "--image_in="~imagePath,
                "--image_out="~imagePath.setExtension("_extracted.jpg"),
                "--image_out_mask="~imagePathExtracted.setExtension("_extracted_mask.jpg"),
                "--image_out_img_text="~imagePathExtracted.setExtension("_extracted_img_text.jpg"),
            ]
            );
        if(extractText.status != 0)
        {
            writeln(extractText.output);
            return recipeData;
        }
        writeln(extractText.output);
        // string testJson = `{"products": [{"name": "1 x7,29 7, 20h", "price": "Eutecz 2 Cze 4009"}, {"name": "{ x2,59 2,69C", "price": "Rogal 7 Days 1106"}, {"name": "2 x9,49 18,98C", "price": "Hielo up szun 200g"}, {"name": "7,00", "price": "Rabat"}, {"name": "1 x12 4) aee", "price": "EarszCzerWiniar60g"}, {"name": "1 x1,59 1,698", "price": "Lleser Des. Mix200g"}, {"name": "0,130 x17,39 2,34C", "price": "Fon malin Pols luz"}, {"name": "1 \u00a5230 27g", "price": "Chleb rodzinny410q"}, {"name": "Fomarancze Hes Luz", "price": "1,240 x5,39 7.43"}, {"name": "=one", "price": "Rabat"}, {"name": "Nandarynka luz", "price": "0,808 x6,99 5,69C"}, {"name": "Rabat", "price": "=3,4"}, {"name": "KinderCountr 23,99", "price": "{1 x1,49 1,498"}, {"name": "DES AVVEG.MIX. 150G", "price": "1. x3,99 3,998"}, {"name": "F LechFreePoned, SL", "price": "1 \u00a52,49 2,498"}, {"name": "Rabat", "price": "\u20140, 50)"}, {"name": "PrzyGyroKanis33q", "price": "{ xt/39 1306"}, {"name": "SPRZEDAZ OPODATKOWANA A", "price": "3,48"}, {"name": "FIU A 23,00 %", "price": "0,65"}, {"name": "SPRZEDAZ OPODATKOWANA B", "price": ""}, {"name": "FTU B 8,00 %", "price": ""}, {"name": "SPRZEDAZ OPODATKOWANA Co", "price": "34,"}, {"name": "FIU C 5,00 %", "price": ""}, {"name": "SUMA PTU", "price": "2,83"}, {"name": "SUMA PLN", "price": "45,26"}]}`;

        JSONSerializer.instance.serialize!(Load.yes, true)(recipeData, extractText.output);
        return recipeData;
    }
}


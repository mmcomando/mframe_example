module main;


import core.thread: Thread, seconds;
import std.algorithm : canFind;
import std.functional : toDelegate;
import std.process : execute;
import std.stdio;

import mframe.controlers.base;
import mframe.controlers.session;
import mframe.controlers.postgresql;
import mframe.db.postgresql;
import controlers.product;
import model.types;

import mframe.controlers.schema;
import mframe.request;
import mframe.router;
import mframe.server;
import mframe.trust;

import mmutils.log;


PostgreSQLAdminController postgreSQLAdminController;
SessionController sessionController;
ProductController productController;

UserSessionData gUserSessionData;// TODO design, global data?
PostgresConnection gConnection;

void developController(ref Router router)
{
    ResponseData responseData;
    RequestData data;
    data.get["query"] = "SELECT datname FROM pg_database;";
    data.get["database"] = "my_data";
    data.get["table"] = "payment";
    data.post["user"] = "user";
    data.post["password"] = "password";
    data.cookie["session"] = "hHGCDWbJJGT8hSl10OyFjiRNaDsWmbbt6kaLumIFHCBS";
    
    // auto controller = router.getControler("/api/postgresql/databases");
    // auto controller = router.getControler("/api/session/login");
    // auto controller = router.getControler("/api/session/get_user_info");
    // auto controller = router.getControler("/api/postgresql/db/my_data/tables");
    // string output = controller(data, responseData);
    // writeln(output);
}

void autoUpdateDatabaseSchema()
{


    string[] queries;
    foreach (schema; gStandardSchemas)
    {
        try
        {
            queries ~= gConnection.getUpdateDatabaseModelQueries(schema);
        }
        catch (Exception e)
        {
            writeln("getUpdateDatabaseModelQueries Exception: ", e);
        }
    }

    foreach (query; queries)
    {
        try
        {
            gConnection.query(query);
        }
        catch (Exception e)
        {
            writeln(e);
        }
    }
}

void userSessionDataInit(ref RequestData requestData)
{
    requestData.userData = &gUserSessionData;
    requestData.trustLevel = TrustLevel.NotAuthenticatedUser;

    string sessionCookie = requestData.cookie.get("session", "");

        requestData.trustLevel = TrustLevel.ApplicationCreator;
    if (sessionCookie.length == 0)
    {
        log(LL(), "Session: NotAuthenticatedUser");
        gUserSessionData = UserSessionData();
        return;
    }
    gUserSessionData = sessionController.cipher.encryptedBase64JSONToType!UserSessionData(sessionCookie);
    requestData.trustLevel = TrustLevel.AuthenticatedUser;
    requestData.userId = gUserSessionData.id;
    log(LL(), "Session: AuthenticatedUser, id(%lld), name(%.*s)", gUserSessionData.id, gUserSessionData.name.length, gUserSessionData.name.ptr);

    if(gUserSessionData.name == "koks")
    {
        requestData.trustLevel = TrustLevel.ApplicationCreator;
    }

}

void startRequiredContainers()
{
    auto cmd = execute(["docker-compose", "up", "-d"]);
    if (cmd.status != 0)
    {
        log(LL(LogType.Fatal), "Starting required containers failed, cmdOutput(%.*s)", cmd.output.length, cmd.output.ptr);
    }
    if(cmd.output.canFind("Creating"))
    {
        Thread.sleep(10.seconds);
    }
}

SchemaGroupApis gStandardSchemaApis;




int main()
{
    mmutils_log_to_file("./www/public/logs.txt", true);
    mmutils_add_log_handler(&mmutils_printf_log_handler);
    log(LL(LogType.Info, LogPriority.VeryImportant), "Application start");

    startRequiredContainers();

    Server server;
    Router router;

    {
        auto perfInitCheck= log_perf(LL(LogType.Info, LogPriority.VeryImportant), "Whole init", "Whole init");
        {
            auto perfCheck = log_perf(LL(LogType.Info, LogPriority.VeryImportant), "Basic init", "Basic init");
            gConnection.initiaizeEnv();
            postgreSQLAdminController.initialize();
            gStandardSchemaApis.initialize(gConnection, gStandardSchemas);
            sessionController.initialize();
            productController.initialize();
            autoUpdateDatabaseSchema();
        }

        {
            auto perfCheck = log_perf(LL(LogType.Info, LogPriority.VeryImportant), "Router register", "Router register");

            router.setRequestDataInitFunc((&userSessionDataInit).toDelegate);
            router.addJSON("/api/ping", &ping, TrustLevel.NotAuthenticatedUser);
            gStandardSchemaApis.register(router, "/api/main/");            
            postgreSQLAdminController.register(router);
            sessionController.register(router);
            productController.register(router);
        }

        {
            auto perfCheck = log_perf(LL(LogType.Info, LogPriority.VeryImportant), "Controler test", "Controler test");
            developController(router);
        }

        server.initialize(router);
    }

    server.start();

    log(LL(LogType.Info, LogPriority.VeryImportant), "Application End");
    return 0;
}

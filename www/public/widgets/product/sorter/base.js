
// import { Template, Widget } from '../../../assets/mframe/js/widget.js';


var expense_id = undefined;

async function refreshColumn(change_el, container_el, widget) {
    container_el.innerHTML = '';
    let req = '';
    if(expense_id)
    {
        req = `?expenseid=${expense_id}`;
    }
    let products = await getApiResponseJson(`/api/product/array${req}`, []);
    // console.log(products);
    for(let prd of products)
    {
        if(prd.productType != change_el.value){
            continue;
        }
        // console.log(prd);
        let product_el = widget.getTemplate('product-element');
        let el = product_el.getElement('text');
        el.innerHTML = prd.name;
        el.dataset.data = JSON.stringify(prd);
        container_el.appendChild(el);
    }
}

async function registerchangeCallback(change_el, container_el, widget) {
    $(change_el).change(async function() {
        refreshColumn(change_el, container_el, widget)
    });
}

async function initializeEventHandler() {
    let column_sorters = [
        document.getElementById('column-1-sorter'),
        document.getElementById('column-2-sorter'),
        document.getElementById('column-3-sorter'),
    ];
    let column_containers = [
        document.getElementById('column-1-container'),
        document.getElementById('column-2-container'),
        document.getElementById('column-3-container'),
    ];

    document.addEventListener('keydown', async function(event) {
        let to_left = true;
        console.log(event);
        switch (event.key) {
            case "ArrowLeft":
                break;
            case "ArrowRight":
                to_left = false;
                break;
            default:
                return;
        }
        
        if(column_containers[1].children.length == 0){
            return;
        }

        let element_to_move = column_containers[1].children[0];
        let product = JSON.parse(element_to_move.dataset.data);
        let colum_sorter_el = to_left ?  column_sorters[0] : column_sorters[2];
        
        product.productType = colum_sorter_el.value;
        element_to_move.dataset.data = JSON.stringify(product);

        if(to_left){
            column_containers[0].appendChild(element_to_move);
        }else{
            column_containers[2].appendChild(element_to_move);
        }
        await getApiResponseJson(`/api/main/sh/product/save`, { id: null }, product, true);
    });
}

export async function render(widget, element) {
    expense_id = element.dataset.expenseid;
    delete element.dataset.expenseid;
    element.innerHTML = '';
    element.appendChild(widget.get_dom());

    let column_sorters = [
        document.getElementById('column-1-sorter'),
        document.getElementById('column-2-sorter'),
        document.getElementById('column-3-sorter'),
    ];
    let column_containers = [
        document.getElementById('column-1-container'),
        document.getElementById('column-2-container'),
        document.getElementById('column-3-container'),
    ];
    registerchangeCallback(column_sorters[0], column_containers[0], widget);
    registerchangeCallback(column_sorters[1], column_containers[1], widget);
    registerchangeCallback(column_sorters[2], column_containers[2], widget);
    refreshColumn(column_sorters[0], column_containers[0], widget);
    refreshColumn(column_sorters[1], column_containers[1], widget);
    refreshColumn(column_sorters[2], column_containers[2], widget);
    await initializeEventHandler();    
}


import { Template, Widget } from '../../../assets/mframe/js/widget.js';

function schemaTypeToTabulatorValidator(schemaType)
{
    switch(schemaType)
    {
        case "string": return null;// Any value can be set
        default:break;
    }
    return schemaType;
}

async function tabulatorTable(apiUrl) {

    let schema = await getApiResponseJson(`${apiUrl}schema`, null);
    if (schema == null) {
        console.error("Faild to get schema. Can not create Tabulator view.");
        return;
    }


    let columns = [
        {
            title: 'Date',
            field: 'time',
            // editor: 'input',
            formatter:function(cell, formatterParams, onRendered){
                //cell - the cell component
                //formatterParams - parameters set for the column
                //onRendered - function to call when the formatter has been rendered
            
                let str =  new Date(cell.getValue() * 1000).toISOString().slice(-13, -5);
                return str;
            },
        },
        {
            title: 'Description',
            field: 'description',
            editor: 'input',
        },
        {
            title: 'Open',

            formatter:function(cell, formatterParams, onRendered){
                //cell - the cell component
                //formatterParams - parameters set for the column
                //onRendered - function to call when the formatter has been rendered
                let id = cell.getRow().getData()['id'];
                let str = `
                <a href="#/" widget-set="product/sorter" widget-target="container-main" data-expenseid="${id}">
                    <span>Expenses
                </a>
                `;
                return str;
            },
            // cellDblClick:function(e, cell){
            //     //e - the click event object
            //     //cell - cell component
            //     console.log(cell);
            // },
        },
    ];
    columns.push({
        formatter: "buttonCross", width: 30, hozAlign: "center", cellClick: function (e, cell) {
            cell.getRow().delete();
        }
    });


    let table = new Tabulator("#example-table", {
        ajaxURL: `${apiUrl}array`,
        layout: "fitColumns",
        columns: columns,
        // persistence:{
        //     page: true, //persist pagination settings
        //     columns: true, //persist column layout
        // },
        // persistenceID:`${apiUrl}`,
        cellEdited: async function (cell) {
            console.log(cell.getData());
            let data = cell.getData();
            for (let key in data) {
                if (data[key] === undefined || data[key] === null) {
                    delete data[key];// do not send undefined values
                }
            }
            let rowElement = cell.getRow().getElement()
            rowElement.style.backgroundColor = "#FBF8EF";// Mark as pending save, TODO use class styles
            let retData = await getApiResponseJson(`${apiUrl}save`, { id: null }, data, true);
            if (retData.id === null) {
                rowElement.style.backgroundColor = "#FF7050";// Mark as error
            } else {
                rowElement.style.backgroundColor = null;// Save ok
            }
            if (retData.id) {
                data.id = retData.id;
            }
            console.log(retData);
            table.updateData([data]);
        },
        rowDeleted: async function (row) {
            let data = row.getData();
            let ok = await getApiResponseOk(`${apiUrl}delete`, { id: data.id }, true);
            console.log(ok);
        },
    });

}

export async function render(widget, element) {
    let apiUrl = '/api/main/sh/expense/';
    let title = widget.getElement('title');
    title.innerHTML = apiUrl;

    element.innerHTML = '';
    element.appendChild(widget.get_dom());

    tabulatorTable(apiUrl);
}

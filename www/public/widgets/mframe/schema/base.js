
import { Template, Widget } from '../../../assets/mframe/js/widget.js';

function schemaTypeToTabulatorValidator(schemaType)
{
    switch(schemaType)
    {
        case "string": return null;// Any value can be set
        default:break;
    }
    return schemaType;
}

async function tabulatorTable(apiUrl) {

    let schema = await getApiResponseJson(`${apiUrl}schema`, null);
    if (schema == null) {
        console.error("Faild to get schema. Can not create Tabulator view.");
        return;
    }


    let columns = [];
    for (let key in schema.properties) {
        let property = schema.properties[key];
        let editable = property.title != "id";
        let title = property.title.replace(/(\w)(\w*)/g, function (g0, g1, g2) { return g1.toUpperCase() + g2.toLowerCase(); }); // To PascalCase
        let col = {
            title: title,
            field: property.title,
            editor: editable,
            validator: schemaTypeToTabulatorValidator(property.type),
            displayOrder: property.displayOrder,
        };
        columns.push(col);
    }
    columns.sort(function (a, b) {
        return a.displayOrder - b.displayOrder;
    });
    for (let val of columns) {
        delete val.displayOrder;// remove fields not used by Tabulator
    }
    columns.push({
        formatter: "buttonCross", width: 30, hozAlign: "center", cellClick: function (e, cell) {
            cell.getRow().delete();
        }
    });


    let table = new Tabulator("#example-table", {
        ajaxURL: `${apiUrl}array`,
        layout: "fitColumns",
        columns: columns,
        // persistence:{
        //     page: true, //persist pagination settings
        //     columns: true, //persist column layout
        // },
        // persistenceID:`${apiUrl}`,
        cellEdited: async function (cell) {
            console.log(cell.getData());
            let data = cell.getData();
            for (let key in data) {
                if (data[key] === undefined || data[key] === null) {
                    delete data[key];// do not send undefined values
                }
            }
            let rowElement = cell.getRow().getElement()
            rowElement.style.backgroundColor = "#FBF8EF";// Mark as pending save, TODO use class styles
            let retData = await getApiResponseJson(`${apiUrl}save`, { id: null }, data, true);
            if (retData.id === null) {
                rowElement.style.backgroundColor = "#FF7050";// Mark as error
            } else {
                rowElement.style.backgroundColor = null;// Save ok
            }
            if (retData.id) {
                data.id = retData.id;
            }
            console.log(retData);
            table.updateData([data]);
        },
        rowDeleted: async function (row) {
            let data = row.getData();
            let ok = await getApiResponseOk(`${apiUrl}delete`, { id: data.id }, true);
            console.log(ok);
        },
    });
    $(document).on("click", ".tabulator-add", function () {
        table.addRow({}, true);
    });

}

export async function render(widget, element) {
    let apiUrl = element.dataset.apiurl;
    let title = widget.getElement('title');
    title.innerHTML = apiUrl;

    element.innerHTML = '';
    element.appendChild(widget.get_dom());

    tabulatorTable(apiUrl);
}

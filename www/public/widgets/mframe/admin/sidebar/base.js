import { Template, Widget } from '../../../../assets/mframe/js/widget.js';


export async function render(widget, element) {
    let schemasNames = await getApiResponseJson('/api/main/schemas', []);
    let container = widget.getElement('container-link');

    for (let schemaName of schemasNames) {
        let link = widget.getTemplate('link');
        let link_obj = link.getElement('link-obj');
        let link_str = link.getElement('link-str');

        link_obj.dataset.apiurl = `/api/main/sh/${schemaName}/`;
        link_str.innerHTML = schemaName;

        container.appendChild(link.template.content);
    }

    let container_databases = widget.getElement('container-databases');
    let link_databases = widget.getElement('link-databases');
    link_databases.onclick = async function () {
        container_databases.innerHTML = '';
        let databases = await getApiResponseJson('/api/postgresql/databases', []);
        for (let database of databases) {
            let link = widget.getTemplate('link-database');
            let link_obj = link.getElement('link-obj');
            let link_str = link.getElement('link-str');
            let container_table = link.getElement('container-table');

            link_str.innerHTML = database;

            link_obj.onclick = async function () {

                container_table.innerHTML = '';
                let tables = await getApiResponseJson(`/api/postgresql/db/${database}/schemas`, []);
                // console.log(tables);
                for (let table of tables) {

                    // let link = widget.getTemplate('link-table');
                    // let link_obj = link.getElement('link-obj');
                    // let link_str = link.getElement('link-str');

                    // link_obj.dataset.table = table;
                    // link_obj.dataset.database = database;
                    // link_str.innerHTML = table;
                    let link = widget.getTemplate('link');
                    let link_obj = link.getElement('link-obj');
                    let link_str = link.getElement('link-str');
            
                    link_obj.dataset.apiurl = `/api/postgresql/db/${database}/sh/${table}/`;
                    link_str.innerHTML = table;

                    container_table.appendChild(link.template.content);
                }
            };

            container_databases.appendChild(link.template.content);
        }
    };

    element.innerHTML = '';
    element.appendChild(widget.get_dom());
}

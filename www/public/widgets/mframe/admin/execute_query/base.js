import {Template, Widget} from '../../../../assets/mframe/js/widget.js';


export async function render(widget, element) {
    let container_result = widget.getElement('container-result');
    let submit_button = widget.getElement('submit-button');
    let input_database = widget.getElement('database');
    let input_query = widget.getElement('query');

    submit_button.onclick  = async function() {
        let sendData = {
            dbName:input_database.value,
            query:input_query.value
        };
        let data = await getApiResponseText('/api/postgresql/execute_query', '', sendData, false);

        container_result.innerHTML = data;;
    };

    let databases = await getApiResponseText('/api/postgresql/databases', '');
    console.log(databases);
    element.appendChild(widget.get_dom());
}
import { Template, Widget } from '../../../../assets/mframe/js/widget.js';

var intervalID = null;

async function refresh(element)
{
    let userInfo = await getApiResponseJson('/api/session/get_user_info', null);
    if(userInfo === null)
    {
        console.error("Can not get user info.");
        return;
    }

    let virtualMemoryMB = (userInfo.procInfo.vsize / 1024 / 1024).toFixed(2);
    let rssMemoryMB = (userInfo.procInfo.rss / 1024 / 1024).toFixed(2);
    let requestsEl = element.querySelector("[data-wname=requests]");
    let virtualMemoryEl = element.querySelector("[data-wname=virtual-memory]");
    let rssMemoryEl = element.querySelector("[data-wname=rss-memory]");
    let titleEl = element.querySelector("[data-wname=title]");
    
    virtualMemoryEl.innerHTML = `${virtualMemoryMB} MB`;
    rssMemoryEl.innerHTML = `${rssMemoryMB} MB`;
    titleEl.innerHTML = `User: ${userInfo.name}`;
    requestsEl.innerHTML = userInfo.procInfo.handledRequests;
}

export async function render(widget, element) {

    element.innerHTML = '';
    element.appendChild(widget.get_dom());

    if(intervalID !== null)
    {
        return;
    }
    refresh(element);
    // intervalID = setInterval(async function(){refresh(element);}, 1000);
}

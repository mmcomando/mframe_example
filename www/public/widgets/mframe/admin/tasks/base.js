
var gTemplateElement = null;


function addQuery(container, query) {
    let el = gTemplateElement.cloneNode(true);
    el.children[0].innerHTML = query;
    container.appendChild(el);
}

async function onWidgetLoad(widget) {
    document.addEventListener('click', async function (e) {
        let element = e.target;
        if (!element.classList.contains('admin-execute-task')) {
            return;
        }
        // let query = "";
        let task = element.innerHTML;
        let data = await getApiResponseOk('/api/execute_task', { task: task });

    }, false);

    gTemplateElement = document.getElementById('admin-tasks-element');
    gTemplateElement.remove();
    gTemplateElement.classList.remove("widget-template");

    let container = widget.getElementsByClassName('container')[0];
    let data = await getApiResponseJson('/api/get_model_queries', []);
    for (query of data) {
        addQuery(container, query);
    }
}

export function register(){
    let widget_data = {
        onWidgetLoad: onWidgetLoad,
    }
    gWidgetsData["admin-tasks"] = widget_data;
}
import {Template, Widget} from './widget.js';

var gWidgetClasses = {};

async function getWidgetClass(widget_name) {
    let widget = gWidgetClasses[widget_name];
    if (widget) {
        return widget;
    }
    console.log(`Initialize widget ${widget_name}`);
    widget = new Widget(widget_name);
    await widget.load();
    gWidgetClasses[widget_name] = widget;
    return widget;
}

async function loadWidget(widget_element) {
    if (widget_element.getAttribute("initialized") == "true") {
        return;
    }

    widget_element.setAttribute("initialized", "true");

    let widget_name = widget_element.getAttribute('widget-name');
    if (!widget_name) {
        console.log("Empty widget name");
        return;
    }
    let id_str = (widget_element.id) ? `to '#${widget_element.id}'` : '';
    console.log(`Load widget data: '${widget_name}' ${id_str}`);
    let widget = await getWidgetClass(widget_name);
    await widget.renderToElement(widget_element);
}

function checkAllWidgets() {
    let elements = document.getElementsByClassName("widget-frame");
    for (let el of elements) {
        let widget_name = el.getAttribute('widget-name');
        if (widget_name == null) {
            continue;
        }
        loadWidget(el);
    }
}

window.addEventListener('load', function () {

    let observer = new MutationObserver(function (mutations) {
        checkAllWidgets();
    });
    observer.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false,
    });


    let elements = document.querySelectorAll('.widget-frame');

    for (let element of elements) {
        loadWidget(element);
    }

    window.addEventListener('click', function (e) {
        let element = e.target.closest('a');
        if (element === null) {
            return;
        }
        let widget_id = element.getAttribute('widget-target');
        let widget_name = element.getAttribute('widget-set');

        if(!widget_id || !widget_name){
            return;
        }

        let widget_element = document.getElementById(widget_id);
        widget_element.setAttribute("initialized", "false");
        widget_element.setAttribute('widget-name', widget_name);
        for(let key of Object.keys(element.dataset)){
            if(key == 'wname')
                continue;
            widget_element.dataset[key] = element.dataset[key];
        }
        loadWidget(widget_element);

    }, false);

});


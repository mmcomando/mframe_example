async function logInUser(){
    let form = document.getElementById('login-form');
    let data = Object.fromEntries(new FormData(form).entries());
    let responsePromise = await getApiResponse('/api/session/login', data, true);
    if(responsePromise.status == 200){
        window.location.replace("/index.html");
    }
}

function validate(){
    new Promise(logInUser);// Async function execution
    return false;// Return immediately
}

function init(){
    document.getElementById('login-form').onsubmit = validate;
}
window.onload = init;
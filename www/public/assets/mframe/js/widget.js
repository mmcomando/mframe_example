export class Template {
    constructor(name, template) {
        this.name = name;
        this.template = template.cloneNode(true);
        this.elements = new Map();
    }

    copy(){
        let temp = new Template(this.name, this.template);
        temp.set_elements();
        return temp
    }

    getElement(name){
        let temp = this.elements.get(name);
        if(!temp){
            console.error(`No element '${name}' in template '${this.name}'`);
            return null;
        }
        return temp;
    }

    set_elements(){
        let templateEditElements = this.template.content.querySelectorAll("[data-wname]");
        for (let el of templateEditElements) {
            let name = el.dataset.wname;
            this.elements.set(name, el);
        }

    }

}

export class Widget {
    constructor(name) {
        this.name = name;
        this.html = '';
        this.module = null;
        this.dom_template = null;
        this.templates = new Map();
        this.elements = new Map();
    }

    copy(){
        let n = new Widget(this.name);
        n.name = this.name;
        n.html = this.html;
        n.module = this.module;
        n.dom_template = this.dom_template.cloneNode(true);
        n.set_elements();
        return n;
    }

    async loadJS() {
        let random_num = '?' + Math.random();
        // let random_num = '';
        let mod = await import(`../../../widgets/${this.name}/base.js${random_num}`);
        return mod;
        
    }

    async getWidgetHTML() {
        try {
            let response = await window.fetch(`/widgets/${this.name}/base.html`, { cache: "no-store" })
            let text = await response.text();
            return text;
        }
        catch (e) {
            console.error(e);
        }
        return '';
    }

    async load() {
        this.module = await this.loadJS();        
        this.html = await this.getWidgetHTML();
        this.dom_template = document.createElement('template');
        this.dom_template.innerHTML = this.html.trim();

        if(this.module.initialize){
            this.module.initialize(this);
        }
    }

    get_dom(){
        return this.dom_template.content;
    }

    set_elements(){
        let dom = this.get_dom();
        let named_elements = dom.querySelectorAll("[data-wname]");

        for (let element of named_elements) {
            let name = element.dataset.wname;

            let parentTemplate = element.closest('template');
            if(!parentTemplate){
                this.elements.set(name, element);
            }
            if(element.tagName != 'TEMPLATE'){
                continue;
            }
            // Add template to template list
            if (this.templates.has(name)) {
                console.error(`Template with name '${name}' already registred in widget '${this.name}'`);
                continue;
            }
            this.templates.set(name, new Template(name, element));

        }
    }

    getElement(name){
        let temp = this.elements.get(name);
        if(!temp){
            console.error(`No element '${name}' in widget '${this.name}'`);
            return null;
        }
        return temp;
    }

    getTemplate(name){
        let temp = this.templates.get(name);
        if(!temp){
            console.error(`No template '${name}' in widget '${this.name}'`);
            return null;
        }
        return temp.copy();
    }

    async renderToElement(element){
        let widgetCopy = this.copy();
        if(!this.module.render){
            console.error(`No function 'render' in JS script in widget '${this.name}'`);
            return;
        }
        element.innerHTML = '';
        this.module.render(widgetCopy, element);
    }


}

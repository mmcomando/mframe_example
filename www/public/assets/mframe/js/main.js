function jsObjectToUrlParametersString(data) {
    if (!data) {
        console.error("Data is not an object");
        console.error(data);
        return '';
    }
    let str = Object.keys(data).map(function (k) {
        return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
    }).join('&');
    return str;
}


async function getApiResponse(requestName, data, isPost) {
    let parameters = data === undefined ? '' : jsObjectToUrlParametersString(data);

    let url = `${requestName}`
    let config = {};
    if (parameters && isPost == false) {
        url += `?${parameters}`;
    }

    if (parameters && isPost) {
        config = {
            method: 'POST',
            body: parameters,
        };
    }

    let response = await fetch(url, config);
    if (response.status != 200) {
        throw `Error Request(${url}), StatusCode(${response.status})`;
    }
    return response;
}

async function getApiResponseJson(requestName, defaultValue, data, isPost) {
    try {
        let response = await getApiResponse(requestName, data, isPost);
        let result = await response.json();
        return result;
    } catch (e) {
        console.error(e);
    }
    return defaultValue;
}

async function getApiResponseText(requestName, defaultValue, data, isPost) {
    try {
        let response = await getApiResponse(requestName, data, isPost);
        let result = await response.text();
        return result;
    } catch (e) {
        console.error(e);
    }
    return defaultValue;
}

async function getApiResponseOk(requestName, data, isPost) {
    try {
        let response = await getApiResponse(requestName, data, isPost);
        console.log(response);
        return true;
    } catch (e) {
        console.error(e);
    }
    return false;
}

